import React from 'react';
import styled from 'styled-components'

import './Person.css'

const StyledDiv = styled.div`
            border: 1px solid red;

            @media (min-width: 300px) {
                width: 500px;
            }`;

const person = (props) => {
    return (
        <StyledDiv>
            <p onClick={props.click}>I am a {props.name} and I am {props.age} years old {props.children}</p>

            <p>{props.children}</p>

            <div>
                <input type='text' onChange={props.changed} />
            </div>
        </StyledDiv>
    )
}

export default person;