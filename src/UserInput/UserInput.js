import React from 'react';

const userInput = (props) => {
    return (
        <div>
            <div className='form-group'>
                <input type='text' onChange={props.changed} value={props.value} />
            </div>
        </div>
    )
}

export default userInput;