import React from 'react';
import Person from './../Person/Person'

import './Example.css'

class Example extends React.Component {
    state = {
        person: [
            { name: "someting", age: 55 },
            { name: 'Stephani', age: 22 }
        ]
    }

    switchNameHandler = (ok) => {
        this.setState({
            person: [
                { name: ok, age: 29 },
                { name: 'Stephani', age: 22 }
            ]
        }
        )
    }

    nameChangedHandler = (event) => {
        console.log(event);

        this.setState({
            person: [
                { name: event.target.value, age: 29 },
                { name: 'Stephani', age: 22 }
            ]
        }
        )
    }

    render() {
        const style = {
            border: '1px solid blue',
            color: 'green'
        };

        return (
            <div style={style}>
                <Person
                    click={this.switchNameHandler.bind(this, 'Example')}
                    name={this.state.person[0].name}
                    age={this.state.person[0].name}
                    changed={this.nameChangedHandler}
                />
            </div>
        );
    }
}

export default Example;