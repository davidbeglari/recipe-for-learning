import React, { useState } from 'react';
import './App.css';
import Radium from 'radium';
import Person from './Person/Person';

import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

import Example from './Example/Example';
import styled from 'styled-components';

function App() {

  const StyleButton = styled.button`
  background-color: ${props => props.alt ? 'red' : 'green'};

  &:hover {
    color: red;
  }
`

  const [person, setPerson] = useState([
    { name: 'Max', age: 25 },
    { name: 'Stephani', age: 22 },
    { name: 'Some', age: 31 },
    { name: 'Good', age: 18 }
  ])

  const [username, setUsername] = useState('My user name');
  const [showPersons, setShowPersons] = useState(false);

  const usernameChangedHandler = (event) => {
    setUsername(event.target.value);
  }

  const nameChangedHandler = (event, name) => {
    const personIndex = person.findIndex(p => {
      return p.name === name
    })


    setPerson([
      { name: event.target.value, age: 29 },
      { name: event.target.value, age: 22 }
    ])
  }

  const togglePersonsHandler = () => setShowPersons(!showPersons)

  let someExample = () => {
    return (
      <div>
        <ul>
          <li>some thing</li>
        </ul>
      </div>
    )
  }

  const deletePersonHandler = (personIndex) => {
    setPerson(person.splice(personIndex, 1));
  }

  const style = {
    color: 'red',
    ':hover': {
      color: 'green'
    }
  }

  const otherStyle = {
    color: 'green'
  }


  const classes = [];

  if (person.length <= 2) {
    classes.push('red');
  }

  if (person.length <= 1) {
    classes.push('bold')
  }




  return (
    <div className="App">

      <StyleButton alt={showPersons}>Good morning style component</StyleButton>

      <div className="col-md-12">
        <UserInput changed={usernameChangedHandler} />

        <UserOutput username={username} />

      </div>

      <p className={classes.join(' ')}>Text for esting dynamic style</p>

      <button
        className="btn btn-primary"
        style={showPersons ? style : otherStyle}
        onClick={() => togglePersonsHandler()}>Show persons</button>




      {showPersons ?
        <div>
          {person.map((item, index) => {
            return <Person
              name={item.name}
              age={item.age}
              key={index}
              click={() => deletePersonHandler(index)}
              changed={(event) => nameChangedHandler(event, item.name)} />
          })}

          <Example />


          <Person name='Good'>Hi</Person>
        </div> : null}

      {showPersons ? someExample() : null}

    </div>
  );
}

export default Radium(App);
